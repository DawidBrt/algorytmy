#include <iostream>
#include <fstream>
#include <string.h>
#include <queue>

using namespace std;

int U, V;

bool bfs(int **rGraph, int s, int t, int parent[]) {

    bool visited[V];
    memset(visited, 0, sizeof(visited));

    queue<int> q;
    q.push(s);
    visited[s] = true;
    parent[s] = -1;

    while (!q.empty()) {
        int u = q.front();
        q.pop();

        for (int v = 0; v < V; v++) {
            if (visited[v] == false && rGraph[u][v] > 0) {
                q.push(v);
                parent[v] = u;
                visited[v] = true;
            }
        }
    }

    return (visited[t] == true);
}

int fordFulkerson(int **graph, int s, int t) {
    //s - sink
    //t - to
    int u, v;

    int **rGraph = new int *[V];
    for (u = 0; u < V; u++) {
        rGraph[u] = new int[V];
        for (v = 0; v < V; v++) {
            rGraph[u][v] = graph[u][v];
        }
    }

    int parent[V];

    int max_flow = 0;

    while (bfs(rGraph, s, t, parent)) {

        int path_flow = 123456789;
        for (v = t; v != s; v = parent[v]) {
            u = parent[v];
            path_flow = min(path_flow, rGraph[u][v]);
        }

        for (v = t; v != s; v = parent[v]) {
            u = parent[v];
            rGraph[u][v] -= path_flow;
            rGraph[v][u] += path_flow;
        }

        max_flow += path_flow;
    }

    return max_flow;
}

int main() {

    ifstream inFile;
    inFile.open("/home/dawid/Project/algorytmy/graf.txt");
    if (!inFile) {
        cerr << "Brak dostepu";
        exit(1);
    }

    inFile >> V;
    inFile >> U;

    int **matrix = new int *[V];

    for (int i = 0; i < V; i++) {
        matrix[i] = new int[V];
        for (int j = 0; j < V; j++) {
            matrix[i][j] = 0;
        }
    }

    for (int i = 0; i < U; i++) {
        int a, b, c;
        inFile >> a;
        inFile >> b;
        inFile >> c;
        matrix[a][b] = c;
    }
    for (int i = 0; i < V; i++) {
        for (int j = 0; j < V; j++) {
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }

    inFile.close(); //wczytany

    cout << "The maximum possible flow is " << fordFulkerson(matrix, 0, 3);


    return 0;
}