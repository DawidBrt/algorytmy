#include <iostream>
#include <fstream>
#include <string.h>
#include <queue>

using namespace std;

int U, V;

bool bellmanFordList(int **graf, int s, int t, int parent[], int weight[], int position[]) {
    int dg[V];
    for (int i = 0; i < V; i++) {
        dg[i] = 123456789;
        parent[i] = -1;
        weight[i] = -1;
        position[i] = -1;
    }
    dg[t] = 0;
    for (int i = 0; i < V - 1; i++) {
        for (int j = 0; j < 2*U; j++) {
            if (dg[graf[j][0]] > (dg[graf[j][1]] + graf[j][2]) && graf[j][2] > 0)  {
                dg[graf[j][0]] = dg[graf[j][1]] + graf[j][2];
                parent[graf[j][0]] = graf[j][1];
                weight[graf[j][0]] = graf[j][2];
                position[graf[j][0]] = j;
            }
        }
    }
    return parent[s] != -1;
}

bool bellmanFordTabela(int **tabela, int s, int t, int parent[]) {

    int dt[V];
    for (int i = 0; i < V; i++) {
        dt[i] = 12345689;
        parent[i] = -1;
    }
    dt[t] = 0;
    for (int i = 0; i < V - 1; i++) {
        for (int j = 0; j < V; j++) {
            for (int k = 0; k < V; k++) {
                if (dt[j] > (dt[k] + tabela[j][k]) && tabela[j][k] > 0) {
                    dt[j] = dt[k] + tabela[j][k];
                    parent[j] = k;
                }
            }
        }
    }
    return parent[s] != -1;
}

int fordFulkersonTabela(int **graph, int s, int t) {
    //s - sink
    //t - to
    int u, v;

    int parent[V];

    int max_flow = 0;

    while (bellmanFordTabela(graph, s, t, parent)) {

        int path_flow = 123456789;
        for (v = s; v != t; v = parent[v]) {
            u = parent[v];
            path_flow = min(path_flow, graph[v][u]);
        }

        for (v = s; v != t; v = parent[v]) {
            u = parent[v];
            graph[v][u] -= path_flow;
            graph[u][v] += path_flow;
        }

        max_flow += path_flow;
    }

    return max_flow;
}

int fordFulkersonList(int **graph, int s, int t) {
    //s - sink
    //t - to
    int u, v, tmp;
    int parent[V];
    int weight[V];
    int position[V];

    int max_flow = 0;

    while (bellmanFordList(graph, s, t, parent, weight, position)) {

        int path_flow = 123456789;

        for (v = s; v != t; v = parent[v]) {
            u = weight[v];
            path_flow = min(path_flow, u);
        }

        for (v = s; v != t; v = parent[v]) {
            u = position[v];
            graph[u][2] -= path_flow;
            tmp = (u + U) % (2*U);
            graph[tmp][2] += path_flow;
        }

        max_flow += path_flow;
    }

    return max_flow;
}

void matrix() {
    ifstream inFile;
    inFile.open("/home/dawid/Project/aglo/algorytmy/matrix.txt");
    if (!inFile) {
        cerr << "Brak dostepu";
        exit(1);
    }

    inFile >> V;

    int **matrix = new int *[V];

    for (int i = 0; i < V; i++) {
        matrix[i] = new int[V];
        for (int j = 0; j < V; j++) {
            inFile >> matrix[i][j];
        }
    }

    for (int i = 0; i < V; i++) {
        for (int j = 0; j < V; j++) {
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }

    inFile.close(); //wczytany

    cout << "The maximum possible flow is " << fordFulkersonTabela(matrix, 0, 3) << endl;
}

void list() {
    ifstream inFile;
    inFile.open("/home/dawid/Project/aglo/algorytmy/list.txt");
    if (!inFile) {
        cerr << "Brak dostepu";
        exit(1);
    }

    inFile >> V;
    inFile >> U;
    int **lista = new int *[2*U];

    for (int i = 0; i < U; i++) {
        lista[i] = new int[3];
        int a, b, c;
        inFile >> a;
        inFile >> b;
        inFile >> c;
        lista[i][0] = a;
        lista[i][1] = b;
        lista[i][2] = c;
        lista[i+U] = new int[3];
        lista[i+U][0] = b;
        lista[i+U][1] = a;
        lista[i+U][2] = 0;
    }

    for (int i = 0; i < 2*U; i++) {
        for (int j = 0; j < 3; j++) {
            cout << lista[i][j] << " ";
        }
        cout << endl;
    }

    inFile.close(); //wczytany

    cout << "The maximum possible flow is " << fordFulkersonList(lista, 0, 3) << endl;
}

int main() {
    matrix();

    cout << "-----------------------" << endl;
    list();

    return 0;
}