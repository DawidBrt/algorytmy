#include <omp.h>
#include <iostream>
#include <random>

using namespace std;

const double a = 10.0;
const long n = 50000000;

bool isInside(double x, double y) {
    double a2 = a / 2;
    double x2 = (x - a2) * (x - a2);
    double y2 = (y - a2) * (y - a2);
    return x2 + y2 <= a2 * a2;
}

void simple() {
    float x, y;
    long square = 0;
    long circle = 0;

    random_device dev;
    mt19937 rng(dev());
    uniform_real_distribution<> dist(0.0, a);

    for (long i = 0; i < n; i++) {
        x = dist(rng);
        y = dist(rng);
        if (isInside(x, y)) {
            circle++;
        }
        square++;
    }
    cout << circle << endl << square << endl << (double) circle / square * 4 << endl;
}

void parallel() { //it is slow - or i have something broken

    double x, y;
    long square = 0;
    long circle = 0;

    random_device dev;
    mt19937 rng(dev());
    uniform_real_distribution<> dist(0.0, a);

#pragma omp parallel for reduction(+:square, circle)
    for (long i = 0; i < n; i++) {
        x = dist(rng);
        y = dist(rng);
        if (isInside(x, y)) {
            circle++;
        }
        square++;
    }
    cout << circle << endl << square << endl << (double) circle / square * 4 << endl;
}

void parallelv2() {
    int sumk;
#pragma omp parallel
    {
        sumk = omp_get_num_threads();
    }
    long square[sumk];
    long circle[sumk];

#pragma omp parallel
    {
        random_device dev;
        mt19937 rng(dev());
        uniform_real_distribution<> dist(0.0, a);

        long s = 0;
        long c = 0;
        int k = omp_get_thread_num();
        float x, y;
        for (long i = k; i < n; i += sumk) {

            x = dist(rng);
            y = dist(rng);
            if (isInside(x, y)) {
                c++;
            }
            s++;
        }
        square[k] = s;
        circle[k] = c;
    }
    long sumS = 0;
    long sumC = 0;
    for (int i = 0; i < sumk; i++) {
        sumS += square[i];
        sumC += circle[i];
    }
    cout << sumC << endl << sumS << endl << (double) sumC / sumS * 4 << endl;
}

int main() {
    cout << "Parallel: " << endl;
    parallelv2();
    cout << "Simple: " << endl;
    simple();

}
