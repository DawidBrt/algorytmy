#include <omp.h>
#include <iostream>
#include <bits/stdc++.h>

using namespace std;

/*
 * policzyc 4 calki od 0 -> 1 z 1/(1+x^2)
 * wynik: pi
 */

double fun(double x) {
    return 1 / (1 + x * x);
}

int main() {
    int sumk;
#pragma omp parallel
    {
        sumk = omp_get_num_threads();
    };
    double wynik[sumk];
    double b = 1;
    double a = 0;
    double dx = 0.000000005;
    long n = (b - a) / dx;

    clock_t start, end;

    double time_taken;
    /* Recording the starting clock tick.*/
    start = clock();

    cout << "Parallel v1:" << endl;
#pragma omp parallel
    {
        int k, sumk;
        k = omp_get_thread_num(); //numer watka
        sumk = omp_get_num_threads(); //liczba watkow
        double w = 0;
        double x = 0;
        for (long i = k; i < n; i+=sumk) {
            x = a + i * dx;
            w += fun(x) * dx;
        }
        wynik[k] = w;
    };

    cout.precision(8);
    double sumaWynik = 0;
    for(int i = 0; i < sumk ; i++) {
        sumaWynik += wynik[i];
    }
    sumaWynik *= 4;
    cout << sumaWynik << endl;

    end = clock();

    time_taken = double(end - start) / double(CLOCKS_PER_SEC) / sumk;
    cout << "Time taken by program is : " << fixed
         << time_taken << setprecision(5);
    cout << " sec " << endl;
    start = clock();
//v2

    cout << "Parallel v2:" << endl;
    double suma = 0;

    double x = 0;
#pragma omp parallel for reduction(+:suma)
    for (long i = 0; i < n; i++) {
        x = a + i * dx;
        suma += fun(x) * dx;
    }

    cout.precision(10);
    suma *= 4;
    cout << suma << endl;
    end = clock();

    time_taken = double(end - start) / double(CLOCKS_PER_SEC) / sumk;
    cout << "Time taken by program is : " << fixed
         << time_taken << setprecision(5);
    cout << " sec " << endl;
    start = clock();
//single


    cout << "Single:" << endl;
    double w = 0;
    for (long i = 0; i < n; i++) {
        x = a + i * dx;
        w += fun(x) * dx;
    }
    w = w * 4;
    cout << w;
    end = clock();

    time_taken = double(end - start) / double(CLOCKS_PER_SEC);
    cout << "Time taken by program is : " << fixed
         << time_taken << setprecision(5);
    cout << " sec " << endl;

    return 0;
}