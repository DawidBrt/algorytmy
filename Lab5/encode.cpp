#include <iostream>
#include <fstream>
#include <string>
#include <queue>
#include <unordered_map>
using namespace std;

void saveTableCode(unordered_map<char, string> code);
void saveCode(string fileName, unordered_map<char, string> code);

// A Tree node
struct Node
{
    char ch;
    int freq;
    Node *left, *right;
};

// Function to allocate a new tree node
Node* getNode(char ch, int freq, Node* left, Node* right)
{
    Node* node = new Node();

    node->ch = ch;
    node->freq = freq;
    node->left = left;
    node->right = right;

    return node;
}

// Comparison object to be used to order the heap
struct comp
{
    bool operator()(Node* l, Node* r)
    {
        // highest priority item has lowest frequency
        return l->freq > r->freq;
    }
};

void encode(Node* root, string str,
            unordered_map<char, string> &huffmanCode)
{
    if (root == nullptr)
        return;

    // found a leaf node
    if (!root->left && !root->right) {
        huffmanCode[root->ch] = str;
    }

    encode(root->left, str + "0", huffmanCode);
    encode(root->right, str + "1", huffmanCode);
}


unordered_map<char, int> setFrequency(string fileName) {
    unordered_map<char, int> freq;
    string line;
    ifstream fileToRead(fileName);
    if (fileToRead.is_open())
    {
        while ( getline (fileToRead,line))
        {
            for (char ch: line) {
                freq[ch]++;
            }
            freq['\n']++; //new line char
        }
        fileToRead.close();
        freq['\n']--; //removing last new line char
        cout << "Frequency set" << endl;
    }
    else {
        cout << "Something wrong with file";
    }

    return freq;
}

void buildHuffmanTree(string fileName)
{
    unordered_map<char, int> freq = setFrequency(fileName);

    // Create a priority queue to store live nodes of Huffman tree;
    priority_queue<Node*, vector<Node*>, comp> pq;

    // Create a leaf node for each character and add it to the priority queue.
    for (auto pair: freq) {
        pq.push(getNode(pair.first, pair.second, nullptr, nullptr));
    }

    // do till there is more than one node in the queue
    while (pq.size() != 1)
    {
        // Remove the two nodes of highest priority
        // (lowest frequency) from the queue
        Node *left = pq.top(); pq.pop();
        Node *right = pq.top();	pq.pop();

        // Create a new internal node with these two nodes
        // as children and with frequency equal to the sum
        // of the two nodes' frequencies. Add the new node
        // to the priority queue.
        int sum = left->freq + right->freq;
        pq.push(getNode('\0', sum, left, right));
    }

    // root stores pointer to root of Huffman Tree
    Node* root = pq.top();

    // traverse the Huffman Tree and store Huffman Codes
    // in a map. Also prints them
    unordered_map<char, string> huffmanCode;

    encode(root, "", huffmanCode);

    saveTableCode(huffmanCode);

    saveCode(fileName, huffmanCode);
}

void saveTableCode(unordered_map<char, string> code) {
    ofstream codeTableFile;
    codeTableFile.open ("/home/dawid/Project/algorytmy/codeTable.txt");
    for (auto pair: code) {
        if (pair.first == '\n') {
            codeTableFile << "\\n " << pair.second << '\n';
        } else {
            codeTableFile << pair.first << " " << pair.second << '\n';
        }
    }
    codeTableFile.close();
    cout << "Table Code saved" << endl;
}

void saveCode(string fileName, unordered_map<char, string> code) {
    string line;
    ifstream fileToRead(fileName);

    ofstream codeFile;
    codeFile.open ("/home/dawid/Project/algorytmy/code.txt");

    if (fileToRead.is_open() && codeFile.is_open())
    {
        while ( getline (fileToRead,line))
        {
            for (char ch: line) {
                codeFile << code[ch];
            }
            codeFile << code['\n']; //new line char
        }
        codeFile.close();
        fileToRead.close();
        cout << "Code saved" << endl;
    }
    else {
        cout << "Something wrong with files";
    }
}


// Huffman coding algorithm
int main()
{
    string file = "/home/dawid/Project/algorytmy/tekst.txt";
    buildHuffmanTree(file);

    return 0;
}