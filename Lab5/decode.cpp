#include <iostream>
#include <fstream>
#include <string>
#include <queue>
#include <unordered_map>

using namespace std;

// A Tree node
struct Node {
    char ch;
    Node *left, *right;
};

Node *getNode(char ch, Node *left, Node *right) {
    Node *node = new Node();

    node->ch = ch;
    node->left = left;
    node->right = right;

    return node;
}

unordered_map<char, string> getTableCode(string fileName) {
    unordered_map<char, string> code;
    string line;
    char codeChar;
    string codeString;
    int lineSize;
    int pos;
    ifstream fileToRead(fileName);

    if (fileToRead.is_open()) {
        while (getline(fileToRead, line)) {
            lineSize = line.size();
            if (lineSize > 0) {
                pos = line.find(' ');
                switch(pos){
                    case 1:
                        codeChar = line.at(0);
                        break;
                    case 2:
                        codeChar = '\n';
                        break;
                    case 0:
                        codeChar = ' ';
                        pos++;
                        break;
                    default:
                        codeChar = line.at(0);
                        break;
                }
                codeString = line.substr(pos + 1);
                code[codeChar] = codeString;
            }
        }
        fileToRead.close();
        cout << "Code table readed" << endl;
    } else {
        cout << "Something wrong with files";
    }
    return code;
}

void saveDecodedChar(string file, char decodedChar) {
    fstream decodeFile;
    decodeFile.open(file, ios::out|ios::app);

    if (decodeFile.is_open()) {
        decodeFile << decodedChar;
        decodeFile.close();
    } else {
        cout << "Something wrong with files";
    }
}

void clearDecoded(string file) {
    ofstream clearFile(file);
    if(clearFile.is_open()) {
        cout << "Cleared";
    }
    clearFile.close();
}

// traverse the Huffman Tree and decode the encoded string
void decode(Node *root, int &index, string str, string file) {
    if (root == nullptr) {
        return;
    }

    // found a leaf node
    if (!root->left && !root->right) {
        saveDecodedChar(file, root->ch);
//        cout << root->ch;
        return;
    }

    index++;

    if (str[index] == '0')
        decode(root->left, index, str, file);
    else
        decode(root->right, index, str, file);
}

string getCodedText(string fileName) {
    string line;
    ifstream fileToRead(fileName);
    if (fileToRead.is_open()) {
        getline(fileToRead, line);

    } else {
        cout << "Something goes wrong with reading coded text";
    }
    return line;
}

void buildHuffmanTree(string code, string table, string decoded) {
    unordered_map<char, string> huffmanCode = getTableCode(table);

    priority_queue<Node *, vector<Node *>> pq;

    pq.push(getNode('\0', nullptr, nullptr));

    Node *root;

    for (auto pair: huffmanCode) {
        root = pq.top();
        for (char c : pair.second) {
            if (c == '1') {
                if (root->right == nullptr) {
                    root->right = getNode('\0', nullptr, nullptr);
                }
                root = root->right;
            } else {
                if (root->left == nullptr) {
                    root->left = getNode('\0', nullptr, nullptr);
                }
                root = root->left;
            }
        }
        root->ch = pair.first;
    }

    root = pq.top();
    // print encoded string
    string str = getCodedText(code);

    int index = -1;
    cout << "\nDecoding\n";

    clearDecoded(decoded);

    while (index < (int) str.size() - 2) {
        decode(root, index, str, decoded);
    }
    cout << "\nDone\n";
}

int main() {
    buildHuffmanTree(
            "/home/dawid/Project/algorytmy/code.txt",
            "/home/dawid/Project/algorytmy/codeTable.txt",
            "/home/dawid/Project/algorytmy/decoded.txt");
}