#include <iostream>

using namespace std;

const int mMax = 123456789;
const int v = 6;
const int u = 8;
const int koncowy = 3;

void bellmanFordGraf() {
    int graf[u][3] = {
            {0, 1, 5},
            {1, 2, 3},
            {2, 3, -1},
            {0, 5, 1},
            {5, 4, 1},
            {4, 3, 3},
            {1, 4, 6},
            {4, 2, -2}
    };
    int dg[v];
    int poprzednik[v];
    for (int i = 0; i < v; i++) {
        dg[i] = mMax;
        poprzednik[i] = -1;
    }
    dg[koncowy] = 0;
    for (int i = 0; i < v - 1; i++) {
        for (int j = 0; j < u; j++) {
            if (dg[graf[j][0]] > (dg[graf[j][1]] + graf[j][2])) {
                dg[graf[j][0]] = dg[graf[j][1]] + graf[j][2];
                poprzednik[graf[j][0]] = graf[j][1];
            }
        }
    }

    for (int j = 0; j < u; j++) {
        if (dg[graf[j][0]] > (dg[graf[j][1]] + graf[j][2])) {
            cout << "ERROR! Cykl o łącznej ujemnej wadze" << endl;
            return;
        }
    }

    for (int i = 0; i < v; i++) {
        cout << i << " D: " << dg[i] << " | Poprzednik: " << poprzednik[i] << endl;
    }
}

void bellmanFordTabela() {
    int tabela[v][v] = {
            {mMax, 5,   mMax, mMax, mMax, 1},
            {mMax, mMax, 3,   mMax, 6,   mMax},
            {mMax, mMax, mMax, -1,  mMax, mMax},
            {mMax, mMax, mMax, mMax, mMax, mMax},
            {mMax, mMax, -2,  3,   mMax, mMax},
            {mMax, mMax, mMax, mMax, 1,   mMax}
    };

    int dt[v];
    int poprzednikTab[v];
    for (int i = 0; i < v; i++) {
        dt[i] = mMax;
        poprzednikTab[i] = -1;
    }
    dt[koncowy] = 0;
    for (int i = 0; i < v - 1; i++) {
        for (int j = 0; j < v; j++) {
            for (int k = 0; k < v; k++) {
                if (dt[j] > (dt[k] + tabela[j][k])) {
                    dt[j] = dt[k] + tabela[j][k];
                    poprzednikTab[j] = k;
                }
            }
        }
    }

    for (int j = 0; j < v; j++) {
        for (int k = 0; k < v; k++) {
            if (dt[j] > (dt[k] + tabela[j][k])) {
                cout << "ERROR! Cykl o łącznej ujemnej wadze" << endl;
                return;
            }
        }
    }

    for (int i = 0; i < v; i++) {
        cout << i << " D: " << dt[i] << " | Poprzednik: " << poprzednikTab[i] << endl;
    }
}

int main() {
    //V - wierzcholoki, E - krawedzie
    //czasowa: O(|V| * |E|)
    //pamieciowa: O(|V|)

    bellmanFordGraf();
    cout << "-------------------" << endl;
    bellmanFordTabela();

    return 0;
}